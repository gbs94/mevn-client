import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {getCleanNumber} from '@/helpers';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		contacts: []
	},
	getters: {
		favourites({contacts}) {
			return contacts.filter(item => item.is_favourite);
		},
		isPhoneUnique({contacts}) {
			return (phone, id) => !contacts.find(
				({phone_number, _id}) => getCleanNumber(phone_number) === getCleanNumber(phone) && _id !== id
			);
		}
	},
	mutations: {
		initContacts(state, result) {
			state.contacts = result;
		},
		createField(state, item) {
			!state.contacts.includes(item) && state.contacts.push(item);
		},
		updateField(state, item) {
			const index = getContactIndex(state, item);
			state.contacts.splice(index, 1, item);
		},
		deleteField(state, _id) {
			const index = getContactIndex(state, {_id});
			state.contacts.splice(index, 1);
		}
	},
	actions: {
		async getAll({commit}) {
			commit('initContacts', await getAllContacts());
		},
		async create({commit}, item) {
			const result = await createContact(item);
			result && commit('createField', result);
		},
		async update({commit}, item) {
			const result = await updateContact(item);
			result && commit('updateField', result);
		},
		async delete({commit}, id) {
			await deleteContact(id) && commit('deleteField', id);
		}
	}
});

async function getAllContacts() {
	try {
		const response = await axios.get('/api/getAll');
		return response.data;
	} catch (error) {
		handleError(error);
		return [];
	}
}

async function createContact(item) {
	try {
		const response = await axios.post('/api/create', item);
		return response.data;
	} catch (error) {
		handleError(error);
		return null;
	}
}

async function updateContact(item) {
	try {
		const response = await axios.put('/api/update', item);
		return response.data;
	} catch (error) {
		handleError(error);
		return null;
	}
}

async function deleteContact(id) {
	try {
		const response = await axios.delete(`/api/delete/${id}`);
		return response.status === 204;
	} catch (error) {
		handleError(error);
		return null;
	}
}

function getContactIndex({contacts}, {_id}) {
	return contacts.findIndex(item => item._id === _id);
}

function handleError(error) {
	if (error.response) {
		console.error(error.response.data);
	} else if (error.request) {
		console.error(error.request);
	} else {
		console.error('Error', error.message);
	}
}
