export function validatePhoneNumber(phone) {
	return /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/.test(phone);
}

export function getCleanNumber(string) {
	return string.replace(/\D+/g, '');
}
